﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{    /// <summary>
     /// Клиенты
     /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        public PreferencesController(IRepository<Preference> preferencesRepository)
        {
            _preferencesRepository = preferencesRepository;
        }
        private IRepository<Preference> _preferencesRepository { get; set; }

        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult<PreferenceResponse>> GetPreferences()
        {
            var preferences = await _preferencesRepository.GetAllAsync();

            return Ok(preferences.Select(p => new PreferenceResponse()
            {
                Id = p.Id,
                Name = p.Name,
            }));
        }
    }
}
